{ pkgs ? import <nixpkgs> {} }:

let
  zmk-python = pkgs.python3.withPackages (ps: with ps; [
    setuptools
    pip
    west

# BASE: required to build or create images with zephyr
#
# While technically west isn't required it's considered in base since it's
# part of the recommended workflow

# used by various build scripts
pyelftools

# used by dts generation to parse binding YAMLs, also used by
# twister to parse YAMLs, by west, zephyr_module,...
pyyaml

# YAML validation. Used by zephyr_module.
pykwalify

# used by west_commands
canopen
packaging
progress
psutil

# for ram/rom reports
anytree

# intelhex used by mergehex.py
intelhex

# it's west
west

  ]);
  gnuarmemb = pkgs.pkgsCross.arm-embedded.buildPackages.gcc;
in
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    git
    wget
    autoconf
    automake
    bzip2
    ccache
    dtc
    dfu-util
    gcc
    libtool
    ninja
    cmake
    xz

    zmk-python

    # keep this line if you use bash
    pkgs.bashInteractive
  ];

  ZEPHYR_TOOLCHAIN_VARIANT = "gnuarmemb";
  GNUARMEMB_TOOLCHAIN_PATH = pkgs.gcc-arm-embedded;
}
